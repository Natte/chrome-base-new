package me.ihaq.chromeclient.command;

import me.ihaq.chromeclient.command.commands.HelpCommand;
import me.ihaq.chromeclient.util.Loader;
import me.ihaq.commandmanager.CommandManager;

public class CommandLoader implements Loader {

    private String prefix = ".";
    private final CommandManager commandManager = new CommandManager(prefix);

    @Override
    public void enable() {
        commandManager.register(new String[]{"h", "help"}, new HelpCommand());
    }

    public CommandManager getCommandManager() {
        return commandManager;
    }

    public String getPrefix() {
        return prefix;
    }
}