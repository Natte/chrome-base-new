package me.ihaq.chromeclient.command.commands;

import com.mojang.realmsclient.gui.ChatFormatting;
import me.ihaq.chromeclient.util.ChatUtils;
import me.ihaq.commandmanager.Command;

import static me.ihaq.chromeclient.Chrome.Managers.COMMAND;

public class HelpCommand implements Command {

    @Override
    public boolean onCommand(String[] args) {
        ChatUtils.sendMessage("Here is a list of all the Commands:");
        COMMAND.getCommandManager().getCommandMap().values().forEach(command -> ChatUtils.sendMessage(command.usage()));
        return true;
    }

    @Override
    public String usage() {
        return "USAGE: " + ChatFormatting.GRAY + "[ " + ChatFormatting.WHITE + "help" + ChatFormatting.GRAY + " ]";
    }
}