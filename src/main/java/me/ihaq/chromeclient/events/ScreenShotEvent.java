package me.ihaq.chromeclient.events;

import me.ihaq.eventmanager.Event;

import java.io.File;

public class ScreenShotEvent extends Event {

    private File screenShot;

    public ScreenShotEvent(File screenShot) {
        this.screenShot = screenShot;
    }

    public File getScreenShot() {
        return screenShot;
    }
}