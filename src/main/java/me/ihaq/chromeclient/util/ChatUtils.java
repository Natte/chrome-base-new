package me.ihaq.chromeclient.util;

import com.mojang.realmsclient.gui.ChatFormatting;
import net.minecraft.client.Minecraft;
import net.minecraft.event.ClickEvent;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IChatComponent;

import static me.ihaq.chromeclient.Chrome.Info.NAME;
import static me.ihaq.chromeclient.Chrome.Managers.COMMAND;

public class ChatUtils {

    private ChatUtils() {

    }

    public static void sendMessage(String text) {
        Minecraft.getMinecraft().ingameGUI.getChatGUI().printChatMessage(new ChatComponentText(ChatFormatting.WHITE + "(" + ChatFormatting.LIGHT_PURPLE + NAME + ChatFormatting.WHITE + ") " + text));
    }

    public static void sendLink(String link) {
        IChatComponent ichat = new ChatComponentText(ChatFormatting.WHITE + "(" + ChatFormatting.LIGHT_PURPLE + NAME + ChatFormatting.WHITE + ") " + link);
        ichat.getChatStyle().setChatClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, link));
        ichat.getChatStyle().setUnderlined(true);
        Minecraft.getMinecraft().ingameGUI.getChatGUI().printChatMessage(ichat);
    }

    public static void sendHelpMessage() {
        sendMessage("Try " + COMMAND.getPrefix() + "help.");
    }
}
