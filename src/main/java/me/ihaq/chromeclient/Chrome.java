package me.ihaq.chromeclient;

import me.ihaq.chromeclient.command.CommandLoader;
import me.ihaq.chromeclient.module.ModuleManager;
import me.ihaq.chromeclient.util.Loader;
import me.ihaq.eventmanager.EventManager;
import me.ihaq.imguruploader.ImgurUploader;
import org.lwjgl.opengl.Display;

import java.util.Arrays;

public class Chrome implements Loader {

    private Loader[] loaders = new Loader[]{new Info(), new Managers(), new Libs()};

    @Override
    public void enable() {
        Arrays.stream(loaders).forEach(Loader::enable);
    }

    @Override
    public void disable() {
        Arrays.stream(loaders).forEach(Loader::disable);
    }

    public static class Info implements Loader {
        public static String NAME = "Chrome";
        public static double VERSION = 1.0;

        @Override
        public void enable() {
            Display.setTitle(NAME + " | " + VERSION);
        }
    }

    public static class Libs implements Loader {
        public static ImgurUploader IMGUR = new ImgurUploader("YOUR_API_KEY");

        @Override
        public void disable() {
            IMGUR.shutdown();
        }
    }

    public static class Managers implements Loader {
        // Non-Loaders
        public static EventManager EVENT = new EventManager();

        // Loader
        public static CommandLoader COMMAND = new CommandLoader();
        public static ModuleManager MODULE = new ModuleManager();

        @Override
        public void enable() {
            invokeLoader(true);
        }

        @Override
        public void disable() {
            invokeLoader(false);
        }

        private void invokeLoader(boolean enable) {
            Arrays.stream(getClass().getDeclaredFields()).forEach(field -> {
                try {
                    field.setAccessible(true);

                    Object obj = field.get(this);
                    if (obj instanceof Loader) {
                        if (enable) {
                            ((Loader) obj).enable();
                        } else {
                            ((Loader) obj).disable();
                        }
                    }

                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            });
        }
    }

}