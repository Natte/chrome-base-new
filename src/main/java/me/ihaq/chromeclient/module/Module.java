package me.ihaq.chromeclient.module;

import com.mojang.realmsclient.gui.ChatFormatting;
import me.ihaq.chromeclient.module.type.Category;
import me.ihaq.chromeclient.util.ChatUtils;
import me.ihaq.chromeclient.util.Timer;
import me.ihaq.eventmanager.listener.EventListener;
import net.minecraft.client.Minecraft;
import net.minecraft.network.Packet;
import org.lwjgl.input.Keyboard;

import static me.ihaq.chromeclient.Chrome.Managers.EVENT;

public class Module implements EventListener {

    protected static final Minecraft mc = Minecraft.getMinecraft();
    protected static final net.minecraft.util.Timer mcTimer = Minecraft.getMinecraft().getTimer();
    protected final Timer timer = new Timer();

    private String name;
    private Category category;
    private int keyCode;
    private String description;
    private boolean toggled;
    private String[] alias;
    private String displayText;

    protected Module(String name, Category category, String description) {
        this(name, category, Keyboard.KEY_NONE, false, description, null);
    }

    protected Module(String name, Category category, int keyCode, boolean toggled, String description, String[] alias) {
        this.name = name;
        this.category = category;
        this.keyCode = keyCode;
        this.toggled = toggled;
        this.description = description;
        this.alias = alias;
    }

    protected void onEnable() {
        timer.reset();
        mcTimer.timerSpeed = 1;
        EVENT.register(this);
    }

    protected void onDisable() {
        timer.reset();
        mcTimer.timerSpeed = 1;
        EVENT.unregister(this);
    }

    protected void sendPacket(Packet packet) {
        mc.thePlayer.sendQueue.addToSendQueue(packet);
    }

    protected void sendMessage(String message) {
        ChatUtils.sendMessage(message);
    }

    protected void sendLink(String link) {
        ChatUtils.sendLink(link);
    }

    public void toggle() {
        if (toggled) {
            onDisable();
        } else {
            onEnable();
        }
        toggled = !toggled;
    }

    public String getDescription() {
        return description;
    }

    public Category getCategory() {
        return category;
    }

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        if (displayText == null) {
            return name;
        } else {
            return name + ChatFormatting.WHITE + " : " + displayText;
        }
    }

    protected void setDisplayText(String displayText) {
        this.displayText = displayText;
    }

    public int getKeyCode() {
        return keyCode;
    }

    public void setKeyCode(int keycode) {
        this.keyCode = keycode;
    }

    public boolean isToggled() {
        return toggled;
    }

    public String[] getAlias() {
        return alias;
    }

}