package me.ihaq.chromeclient.module;

import me.ihaq.chromeclient.events.KeyPressEvent;
import me.ihaq.chromeclient.module.modules.ScreenShot;
import me.ihaq.chromeclient.module.type.Category;
import me.ihaq.chromeclient.util.Container;
import me.ihaq.chromeclient.util.Loader;
import me.ihaq.eventmanager.listener.EventListener;
import me.ihaq.eventmanager.listener.EventTarget;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static me.ihaq.chromeclient.Chrome.Managers.EVENT;

public class ModuleManager extends Container<Module> implements Loader, EventListener {

    @Override
    public void enable() {
        EVENT.register(this);
        add(new ScreenShot());
    }

    public Module getModule(Class<? extends Module> clazz) {
        return getContents().stream()
                .filter(module -> module.getClass() == clazz)
                .findFirst().orElse(null);
    }

    public Module getModule(String name) {
        return getContents().stream()
                .filter(module -> module.getName().equalsIgnoreCase(name) || (module.getAlias() != null && Arrays.asList(module.getAlias()).contains(name)))
                .findFirst().orElse(null);
    }

    public List<Module> getModules(Category category) {
        return getContents().stream()
                .filter(module -> module.getCategory() == category)
                .collect(Collectors.toList());
    }

    public List<Module> getToggledModules() {
        return getContents().stream()
                .filter(Module::isToggled)
                .collect(Collectors.toList());
    }

    @EventTarget
    public void onKeyPress(KeyPressEvent e) {
        getContents().stream()
                .filter(module -> e.getKey() == module.getKeyCode())
                .forEach(Module::toggle);
    }
}
