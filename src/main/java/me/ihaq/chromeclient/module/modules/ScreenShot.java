package me.ihaq.chromeclient.module.modules;

import com.google.gson.JsonObject;
import me.ihaq.chromeclient.events.ScreenShotEvent;
import me.ihaq.chromeclient.module.Module;
import me.ihaq.chromeclient.module.type.Category;
import me.ihaq.eventmanager.listener.EventTarget;
import me.ihaq.imguruploader.util.Callback;

import static me.ihaq.chromeclient.Chrome.Libs.IMGUR;

public class ScreenShot extends Module {

    public ScreenShot() {
        super("ScreenShot", Category.WORLD, "Uploads your screenshot to imgur.");
    }

    @EventTarget
    public void onScreenShot(ScreenShotEvent e) {
        sendMessage("Uploading...");
        IMGUR.uploadAsync(new Callback() {
            @Override
            public void onSuccess(JsonObject e) {
                sendLink(e.get("link").getAsString());
            }

            @Override
            public void onFail(Exception e) {
                sendMessage("Error uploading image.");
            }
        }, e.getScreenShot());
    }
}
